<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::resource('object', KeyValueController::class);
Route::get('object/get_all_records', 'KeyValueController@get_all');
Route::get('object/{param}', 'KeyValueController@index');
Route::post('object', 'KeyValueController@create_or_update');
