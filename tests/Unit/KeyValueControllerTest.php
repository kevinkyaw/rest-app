<?php

namespace Tests\Unit;
use Tests\TestCase;
use Faker\Factory;
use Illuminate\Support\Str;
use App\Models\KeyValue;

class KeyValueControllerTest extends TestCase
{
    protected $payload = [];
    protected $failPayload = [];
    protected $exceptionPayload = [];
    protected $invalidKeyPayload = [];
    protected $faker;

    public function setUp() : void 
    {   
        parent::setUp(); 
        $this->payload = [
            ['key'=>'value'],
            ['key_1'=>'value_1'],
            ['key_2'=>'value_2'],
            ['key_3'=>'value_3']
        ];
        $this->faker = Factory::create();
        $this->failPayload = [
            $this->faker->word() => $this->faker->word(),
            $this->faker->word() => $this->faker->word(),
            $this->faker->word() => $this->faker->word(),
        ];
        $this->exceptionPayload = [
            str_repeat((string) Str::uuid(), 50)=>  (string) Str::uuid()
        ];

        $this->invalidKeyPayload = [
            1 => 'Hello World'
        ];
    }

    /**
     * @test
     * @return void
     */
    public function create_or_update_record_success()
    {
        foreach($this->payload as $key => $payload)
        {   
            $result = $this->json('POST', 'api/object', $payload, ['Accept' => 'application/json']);
            
            $result->assertStatus(200);
            $result->assertJson([
                "result" => "Nicely done!"
            ]);
        }
        
    }

    /**
     * @test
     * @return void
     */
    public function create_or_update_record_fail_with_exception()
    {
        $result = $this->json('POST', 'api/object', $this->exceptionPayload, ['Accept' => 'application/json']);
        $result->assertStatus(200);
        $result->assertJson([
            "result" => "Ooops, there is a issue. Please try again!"
        ]);
    }

    /**
     * @test
     * @return void
     */
    public function get_individual_record_success() {
        
        $result = $this->json('GET', "api/object/key");
        $result->assertJson([
            "result" => [
                "key" => "value"
            ]
        ]);
    }

    /**
     * @test
     * @return void
     */
    public function create_or_update_record_fail()
    {
        $result = $this->json('POST', 'api/object', $this->failPayload, ['Accept' => 'application/json']);
        $result->assertStatus(200);
        $result->assertJson([
            "result" => "Invalid key value"
        ]);
    }

    /**
     * @test
     * @return void
     */
    public function create_or_update_record_fail_with_invalid_key()
    {
        $result = $this->json('POST', 'api/object', $this->invalidKeyPayload, ['Accept' => 'application/json']);
        $result->assertStatus(200);
        $result->assertJson([
            "result" => "Invalid Key"
        ]);
    }

    /**
     * @test
     * @return void
     */
    public function get_all_records() {
        $result = $this->json('GET', 'api/object/get_all_records');
        
        $result->assertJsonStructure([
            "result" => [
                '*' => [
                    "body",
                    "timestamp"
                ]
            ]
        ]);
    }

    /**
     * @test
     * @return void
     */
    public function get_individual_record_fail() {
        
        $result = $this->json('GET', sprintf("api/object/%s",$this->faker->word()));
        $result->assertJson([
            "result" => []
        ]);
    }

    /**
     * @test
     * @return void
     */
    public function get_individual_record_with_timestamp_empty_record() {
        $result = $this->json('GET', sprintf("api/object/key_3?timestamp=%d",time()));
        $result->assertJson([
            "result" => []
        ]);
    }   

    /**
     * @test
     * @return void
     */
    public function get_individual_record_with_timestamp() {
        $object = KeyValue::where('column_name', 'key_1')
            ->orderBy('id', 'desc')
            ->first();
        
        $result = $this->json('GET', sprintf("api/object/key_1?timestamp=%d",$object['timestamp']));
        $result->assertJson([
            "result" => [
                "key_1" => "value_1"
            ]
        ]);
    }   
}
