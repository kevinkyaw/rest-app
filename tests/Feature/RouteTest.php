<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * @test
     * @return void
     */
    public function check_the_base_route()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * @test
     * @return void
     */
    public function check_url_is_not_exist()
    {
        $response = $this->get('/api');

        $response->assertStatus(404);
    }

    /**
     * @test
     * @return void
     */
    public function check_the_exist_url_with_get_method_and_key()
    {
        $response = $this->get('/api/object/key');

        $response->assertStatus(200);
    }

    /**
     * @test
     * @return void
     */
    public function check_the_exist_url_with_get_method_and_key_and_timestamp()
    {
        $response = $this->get('/api/object/key?timestamp='.time());

        $response->assertStatus(200);
    }

    /**
     * @test
     * @return void
     */
    public function check_the_url_for_get_all_records()
    {
        $response = $this->get('/api/object/get_all_records');

        $response->assertStatus(200);
    }

    /**
     * @test
     * @return void
     */
    public function check_route_for_post()
    {
        $response = $this->post('/api/object');

        $response->assertStatus(200);
    }

    /**
     * @test
     * @return void
     */
    public function check_invalid_methods()
    {
        $response = $this->put('/api/object');
        $response->assertStatus(405);

        $response = $this->delete('/api/object');
        $response->assertStatus(405);
    }
}