<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KeyValue extends Model
{
    use HasFactory;
    protected $table = 'key_values';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'uuid',
        'column_name',
        'body',
        'timestamp'
    ];

    public function getExistingKeyValuePair(string $key) {
        return KeyValue::where('column_name', $key)->orderBy('id', 'desc')->first() ?? [];
    }
}
