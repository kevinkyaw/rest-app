<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Str;
use Carbon\Carbon as Carbon;
use App\Models\KeyValue;
use Illuminate\Database\QueryException as QueryException;
use \Exception as Exception;

class KeyValueController extends Controller
{
    /**
     * Display a listing of the record when condition is meet.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($param,Request $request)
    {
        $timestamp = $request->get('timestamp');
        
        if(is_null($timestamp)) {
            $object = KeyValue::getExistingKeyValuePair($param);
        }else{
            $object = KeyValue::select('body')
                ->where('column_name', $param)
                ->where('timestamp',$timestamp)
                ->orderBy('id','desc')
                ->first() ?? [];
        }

        if(!empty($object)) {
            return response(['result' => json_decode($object['body'])]);
        }
        
        return response(['result' => [ __('No record found!') ]]);
        
    }

    /**
     * Create or update record
     *
     * @return \Illuminate\Http\Response
     */
    public function create_or_update(Request $request)
    {
        if(count($request->all()) > 1) return response(['result' => __('Invalid key value')]);
        $key = key($request->all());
        if(!is_string($key)) return response(['result' => __('Invalid Key')]);
        $status = false;
        $object = KeyValue::getExistingKeyValuePair($key);
        
        try{
            if(empty($object)) {    
                $status = KeyValue::insert([
                    'uuid' => Str::uuid(), 
                    'column_name' => $key,
                    'body' => json_encode($request->all()),
                    'timestamp' => time(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }else{
                $status = KeyValue::where('column_name', '=', $key)->where('id','=',$object['id'])
                    ->update(['body'=>json_encode($request->all()),'timestamp'=>time()]);
            }            
        }catch(QueryException | Exception $e) {
            \Sentry\captureException($e);
        }
        
        return response(['result' => ($status) ? __('Nicely done!'): __('Ooops, there is a issue. Please try again!')]);
    }

     /**
     * Display all listing of the records.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_all()
    {
        $object = KeyValue::select('body','timestamp')->get() ?? [];
        
        if(!empty($object)) {
            $result = [];
            foreach($object as $obj) {
                $tmp = [];
                $tmp['body'] = json_decode($obj['body']);
                $tmp['timestamp']= $obj['timestamp'];
                $result[] = $tmp;
            }
            return response(['result' => $result]);
        }
        
        return Response::json(['result' => []]);
    }
}
