# Requirements
  - PHP >= 7.4
  - Larvel 8
  - Mysql
  - Composer
  - phpunit 

# Using your local machine

1. Install project and dependencies:
    Clone the repo from gitlab
    `git clone git@gitlab.com:kevinkyaw/rest-app.git` 
    Go to project folder and run
    `composer install`
    `php artisan migrate`

2. Run the project
    `php artisan serve`

3. Run the test
    `vendor/bin/phpunit`

4. Available endpoint 
    #### To create new record or update the existing records
    - Method: POST
    - Endpoint: http://yourhostname/api/object
    - Body: JSON: {your_key : your_value}
    
    #### To get record for provided key
    - Method: GET
    - Endpoint: http://yourhostname/api/object/your_key
    - Response: your_value object

    ### To get all the existing records
    - Method: GET
    - Endpoint: http://yourhostname/api/object/get_all_records
    - Response: JSON array of all existing records

    #### To get record for provided key that based on timestamp
    - Method: GET
    - Endpoint: http://yourhostname/api/object/your_key?timestamp=1440568980
    - Response: your_value object
